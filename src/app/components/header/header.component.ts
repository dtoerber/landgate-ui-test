import { Component, OnInit } from '@angular/core';
import { NgbNavConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  isNavbarCollapsed = true;
  constructor(config: NgbNavConfig) {
    config.destroyOnHide = false;
    config.roles = false;
  }

  collapseNavBar() {
    this.isNavbarCollapsed = true;
  }

  ngOnInit() {}
}

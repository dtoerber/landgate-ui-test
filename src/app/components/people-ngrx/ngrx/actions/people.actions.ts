import { createAction, props } from '@ngrx/store';
import { PersonModel } from '../../../../models/person-model';

// This is a request
export const load = createAction('[People] Load');

// Success
export const loadSuccess = createAction(
  '[People] Load Success',
  props<{ people: PersonModel[] }>()
);

// Error
export const loadError = createAction(
  '[People] Load Error',
  props<{ error: any }>()
);

// Update Person
export const updatePerson = createAction(
  '[People] Update Person',
  props<{ person: PersonModel }>()
);

// Cancel Update
export const cancel = createAction('[People] Cancel');

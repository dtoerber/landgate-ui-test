import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';

import { switchMap, map, catchError } from 'rxjs/operators';
import { EMPTY, of } from 'rxjs';

import * as fromActions from '../actions/people.actions';
import { PeopleService } from 'src/app/services/people.service';

@Injectable()
export class PeopleEffects {
  loadPeoples$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(fromActions.load),
      // TODO Implement a Load People Action that gets the data from the service.
      switchMap(() =>
        this.peopleService.getPeople().pipe(
          map((people) => {
            // console.log(people);
            return fromActions.loadSuccess({ people });
          }),
          catchError((error) => of(fromActions.loadError({ error })))
        )
      )
    );
  });

  constructor(
    private actions$: Actions,
    private peopleService: PeopleService
  ) {}
}

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { select, Store } from '@ngrx/store';
import * as fromActions from './actions/people.actions';
import * as fromReducer from './reducers/people.reducer';
import * as fromSelectors from './selectors/people.selectors';
import { PersonModel } from '../../../models/person-model';

@Injectable({ providedIn: 'root' })
export class PeopleFacade {
  people$: Observable<PersonModel[]> = this.store.pipe(
    select(fromSelectors.selectAllPeople)
  );

  getPerson$(id: string): Observable<PersonModel> {
    return this.store.pipe(select(fromSelectors.selectPerson(id)));
  }
  constructor(private store: Store<fromReducer.State>) {}

  load() {
    this.store.dispatch(fromActions.load());
  }

  update(person: PersonModel) {
    this.store.dispatch(fromActions.updatePerson({ person }));
  }

  cancel() {
    this.store.dispatch(fromActions.cancel());
  }
}

import { Action, createReducer, on } from '@ngrx/store';
import { EntityState, createEntityAdapter } from '@ngrx/entity';

import * as fromActions from '../actions/people.actions';
import { PersonModel } from '../../../../models/person-model';
import { PersonDetailsComponent } from '../../person-details/person-details.component';

export const peopleFeatureKey = 'people';

// create Entity adapter
export const peopleAdapter = createEntityAdapter<PersonModel>();

export interface State extends EntityState<PersonModel> {}

export const initialState: State = peopleAdapter.getInitialState({});

const peopleReducer = createReducer(
  initialState,
  on(fromActions.loadSuccess, (state, { people }) => {
    return peopleAdapter.setAll(people, {
      ...state,
      selectedPerson: null,
    });
  }),
  on(fromActions.updatePerson, (state, { person }) => {
    const id = person.id;
    const changes = person;
    return peopleAdapter.updateOne({ id, changes }, state);
  })
);

export function reducer(state: State | undefined, action: Action) {
  return peopleReducer(state, action);
}

// Create the default selectors
export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = peopleAdapter.getSelectors();

import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromReducer from '../reducers/people.reducer';

export const selectPeopleState = createFeatureSelector<fromReducer.State>(
  fromReducer.peopleFeatureKey
);

// TODO: need to add a selector for people.
export const selectPeopleIds = createSelector(
  selectPeopleState,
  fromReducer.selectIds
);

export const selectPeopleEntities = createSelector(
  selectPeopleState,
  fromReducer.selectEntities
);

export const selectAllPeople = createSelector(
  selectPeopleState,
  fromReducer.selectAll
);

export const selectPerson = (id: string) =>
  createSelector(selectAllPeople, (entities) => {
    return entities && entities.find((e) => e.id === id);
  });

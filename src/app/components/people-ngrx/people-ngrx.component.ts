import { Component } from '@angular/core';
import { PeopleFacade } from './ngrx/people.facade';

@Component({
  selector: 'app-people-component',
  templateUrl: './people-ngrx.component.html',
  styleUrls: ['./people-ngrx.component.scss'],
})
export class PeopleNgrxComponent {
  // TODO: Include the Store and get the data from the NgrxStore
  constructor(public people: PeopleFacade) {}
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PeopleNgrxComponent } from './people-ngrx.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import * as fromPeople from './ngrx/reducers/people.reducer';
import { PeopleEffects } from './ngrx/effects/people.effects';
import { PersonDetailsComponent } from './person-details/person-details.component';
import { ReactiveFormsModule } from '@angular/forms';
import { PeopleFacade } from './ngrx/people.facade';

@NgModule({
  declarations: [PeopleNgrxComponent, PersonDetailsComponent],
  exports: [PeopleNgrxComponent],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    StoreModule.forFeature(fromPeople.peopleFeatureKey, fromPeople.reducer),
    EffectsModule.forFeature([PeopleEffects]),
  ],
})
export class PeopleNgrxModule {
  constructor(public people: PeopleFacade) {
    this.people.load();
  }
}

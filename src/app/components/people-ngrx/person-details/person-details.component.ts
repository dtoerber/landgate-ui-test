import { Component, OnInit } from '@angular/core';
import { filter, first } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { PersonModel } from 'src/app/models/person-model';
import { PeopleFacade } from '../ngrx/people.facade';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-person-details',
  templateUrl: './person-details.component.html',
  styleUrls: ['./person-details.component.scss'],
})
export class PersonDetailsComponent implements OnInit {
  person: PersonModel;
  personForm: FormGroup;

  constructor(
    private people: PeopleFacade,
    private route: ActivatedRoute,
    private fb: FormBuilder
  ) {
    this.createForm();
  }

  ngOnInit() {
    // This page assumes the people are already loaded.
    const id = this.route.snapshot.params.id;
    this.people
      .getPerson$(id)
      .pipe(
        filter((person) => !!person),
        first()
      )
      .subscribe((person) => {
        this.person = person;
        this.personForm.patchValue(this.person);
      });
  }

  createForm() {
    this.personForm = this.fb.group({
      id: '',
      firstName: '',
      lastName: '',
      age: '',
      jobTitle: '',
    });
  }

  save(person: PersonModel) {
    this.people.update(person);
  }
}

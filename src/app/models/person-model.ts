import * as uuid from 'uuid';

export class PersonModel {
  public id: string;
  public firstName: string;
  public lastName: string;
  public age: number;
  public jobTitle: string;

  constructor(dataIn: any) {
    // TODO: Implement a dataIn object that gets passed in as a JavaScript
    this.id = uuid.v4();
    this.firstName = dataIn.firstName;
    this.lastName = dataIn.lastName;
    this.age = JSON.parse(dataIn.age);
    this.jobTitle = dataIn.workTitle;
  }
}
